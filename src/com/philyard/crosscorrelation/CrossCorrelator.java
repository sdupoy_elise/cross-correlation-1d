package com.philyard.crosscorrelation;

public class CrossCorrelator {

    public int crossCorrelate(double[] signal1, double[] signal2) {

        // to avoid periodicity issues (wraparound error) we pad to (signal1.len + signal2.len - 1)
        int paddedLength = signal1.length + signal2.length - 1;

        // discrete fft algorithms require input lengths equal to a second power (because they effectively recursively binary chop)
        int fftLength = this.getNextSecondPower(paddedLength) * 2;

        // get complex numbers
        ComplexSeries complex1 = new ComplexSeries(signal1, fftLength);
        ComplexSeries complex2 = new ComplexSeries(signal2, fftLength);

        // convert from spatial to frequency domain
        complex1.fourierTransform();
        complex2.fourierTransform();

        // cross-correlate in the frequency domain
        complex1.conjugate();
        complex1.multiplyBy(complex2);

        // convert from frequency back into spatial domain
        complex1.inverseFourierTransform();

        // SCOTT: TODO: need to ensure it's a clear peak (compare against second best peak with ratio?)
//        for (int i = 0; i < complex1.getComplexSeries().length; i++) {
//            System.out.println(String.format("%5d - %f", i, complex1.getComplexSeries()[i].getReal()));
//        }

        // work out where the peak is
        int peakIndex = this.getPeakIndex(complex1);

        // if it's after our original signal range then signal2 is ahead of signal1 so switch to a negative index
        if (peakIndex >= signal1.length) {
            peakIndex = peakIndex - complex1.getComplexSeries().length;
        }

        return peakIndex;
    }

    private int getNextSecondPower(int length) {
        int paddedLength = 1;
        while (paddedLength < length) {
            paddedLength *= 2;
        }
        return paddedLength;
    }

    private int getPeakIndex(ComplexSeries series) {
        double peak = Double.MIN_VALUE;
        int peakIndex = 0;
        Complex[] array = series.getComplexSeries();
        for (int i = 0; i < array.length; i++) {
            Complex complex = array[i];
            if (complex.getReal() > peak) {
                peak = complex.getReal();
                peakIndex = i;
            }
        }
        return peakIndex;
    }
}
