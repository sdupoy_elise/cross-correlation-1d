package com.philyard.crosscorrelation;

public class Complex {

    private double real;
    private double imaginary;

    // create a new object with the given real and imaginary parts
    public Complex(double real, double imag) {
        this.real = real;
        this.imaginary = imag;
    }

    public double getReal() { return this.real; }
    public double getImaginary() { return this.imaginary; }

    public void set(double real, double imaginary) {
        this.real = real;
        this.imaginary = imaginary;
    }

    public void conjugate() {
        // in-place conjugation
        this.imaginary = -this.imaginary;
    }

    public void multiplyBy(Complex complex) {
        // in-place complex multiplication
        double multipliedReal = this.real * complex.real - this.imaginary * complex.imaginary;
        double multipliedImaginary = this.real * complex.imaginary + this.imaginary * complex.real;
        this.real = multipliedReal;
        this.imaginary = multipliedImaginary;
    }

    public void multiplyBy(double scalar) {
        // in-place scalar multiplication
        this.real *= scalar;
        this.imaginary *= scalar;
    }
}