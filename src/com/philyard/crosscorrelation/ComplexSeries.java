package com.philyard.crosscorrelation;

public class ComplexSeries {

    private Complex[] series;

    public ComplexSeries(double[] input, int paddedLength) {
        // SCOTT: TODO: assert paddedLength is a power of 2
        this.series = new Complex[paddedLength];
        for (int i = 0; i < paddedLength; i++) {
            if (i < input.length) {
                this.series[i] = new Complex(input[i], 0.0);
            }
            else {
                this.series[i] = new Complex(0.0, 0.0);
            }
        }
    }

    public Complex[] getComplexSeries() {
        return this.series;
    }

    public void conjugate() {
        for (int i = 0; i < this.series.length; i++) {
            this.series[i].conjugate();
        }
    }

    public void multiplyBy(ComplexSeries rhs) {
        if (this.series.length != rhs.series.length) {
            throw new IllegalArgumentException("series must be of same length");
        }
        for (int i = 0; i < this.series.length; i++) {
            this.series[i].multiplyBy(rhs.series[i]);
        }
    }

    private void multiplyBy(double scalar) {
        for (int i = 0; i < this.series.length; i++) {
            this.series[i].multiplyBy(scalar);
        }
    }

    public void inverseFourierTransform() {
        this.conjugate();
        this.fourierTransform();
        this.conjugate();
        this.multiplyBy(1.0 / this.series.length);
    }

    public void fourierTransform() {

        // rearrange before processing which removes the need to recursively divide and conquer
        int bits = (int) (Math.log(this.series.length) / Math.log(2)); // == ln(buffer.length)
        for (int j = 1; j < this.series.length; j++) {
            int swapPos = this.bitReverse(j, bits);
            if (swapPos > j) {
                // swap the elements
                Complex temp = this.series[j];
                this.series[j] = this.series[swapPos];
                this.series[swapPos] = temp;
            }
        }

        for (int N = 2; N <= this.series.length; N <<= 1) {
            for (int i = 0; i < this.series.length; i += N) {
                for (int k = 0; k < N / 2; k++) {

                    int evenIndex = i + k;
                    int oddIndex = i + k + (N / 2);
                    Complex even = this.series[evenIndex];
                    Complex odd = this.series[oddIndex];

                    double term = (-2.0 * Math.PI * k) / (double)N;
                    double realMultiplier = Math.cos(term);
                    double imaginaryMultiplier = Math.sin(term);

                    double expReal = realMultiplier * odd.getReal() - imaginaryMultiplier * odd.getImaginary();
                    double expImaginary = realMultiplier * odd.getImaginary() + imaginaryMultiplier * odd.getReal();

                    double previousEvenReal = even.getReal();
                    double previousEvenImaginary = even.getImaginary();

                    even.set(previousEvenReal + expReal, previousEvenImaginary + expImaginary);
                    odd.set(previousEvenReal - expReal, previousEvenImaginary - expImaginary);
                }
            }
        }
    }

    private int bitReverse(int n, int bits) {
        int reversedN = n;
        int count = bits - 1;

        n >>= 1;
        while (n > 0) {
            reversedN = (reversedN << 1) | (n & 1);
            count--;
            n >>= 1;
        }

        return ((reversedN << count) & ((1 << bits) - 1));
    }
}
