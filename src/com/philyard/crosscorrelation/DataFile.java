package com.philyard.crosscorrelation;

public class DataFile {

    private String symbol;
    private String feed;
    private double[] signal;
    private double[] deltaSignal;

    public DataFile(String symbol, String feed, double[] signal) {
        this.symbol = symbol;
        this.feed = feed;
        this.signal = signal;
        this.deltaSignal = this.buildDeltaSignal(signal);
    }

    public String getSymbol() { return this.symbol; }
    public String getFeed() { return this.feed; }
    public double[] getSignal() { return this.signal; }
    public double[] getDeltaSignal() { return this.deltaSignal; }

    private double[] buildDeltaSignal(double[] input) {
        double[] deltas = new double[input.length];
        deltas[0] = 0.0;
        double previous = input[0];
        for (int i = 1; i < input.length; i++) {
            double next = input[i];
            deltas[i] = next - previous;
            previous = next;
        }
        return deltas;
    }
}
