package com.philyard.crosscorrelation;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DataFileReader {

    public Map<String, List<DataFile>> readFiles(String directory, String ending) {

        System.out.println("Scanning directory:" + directory + ", ending: " + ending);
        Map<String, List<DataFile>> dataFiles = new HashMap<>();

        for (File file : new File(directory).listFiles((d, name) -> name.endsWith(ending))) {

            String symbol = this.getSymbol(file.getName());
            String feed = this.getFeed(file.getName());

            if (symbol.equals("RTY") || feed.endsWith("_ALTERNATE")) {
                continue;
            }

            System.out.println("Reading file: " + file.getName() + ", symbol: " + symbol + ", feed: " + feed);

            List<DataFile> symbolDataFiles = dataFiles.get(symbol);
            if (symbolDataFiles == null) {
                symbolDataFiles = new ArrayList<>();
                dataFiles.put(symbol, symbolDataFiles);
            }

            symbolDataFiles.add(new DataFile(symbol, feed, this.readFile(file)));
        }

        return dataFiles;
    }

    private String getSymbol(String fileName) {
        return fileName.substring(0, fileName.indexOf('-'));
    }

    private String getFeed(String fileName) {
        int start = fileName.indexOf('-') + 1;
        int end = fileName.indexOf('-', start);
        return fileName.substring(start, end);
    }

    private double[] readFile(File file) {
        List<Double> values = new ArrayList<>(); // SCOTT: inefficient!

        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            boolean firstLine = true;
            while ((line = reader.readLine()) != null) {
                if (!firstLine) {
                    values.add((double) Integer.parseInt(line.substring(line.lastIndexOf(',') + 1)));
                }
                firstLine = false;
            }
        }
        catch (IOException ioe) {
            // eat
            System.out.println("IOE: " + ioe.getMessage());
        }

        // no, no, no!
        double[] array = new double[values.size()];
        for (int i = 0; i < values.size(); i++) {
            array[i] = values.get(i);
        }

        return array;
    }
}
