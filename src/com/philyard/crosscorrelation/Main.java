package com.philyard.crosscorrelation;

import java.util.List;
import java.util.Map;

public class Main {

    public static void main(String[] args) {
        //test();
        run();
    }

    private static void run() {
        //
        // expecting file name format to be symbol-feed-date.csv, eg: NG-TT_MD-20190919.csv
        //
        CrossCorrelator crossCorrelator = new CrossCorrelator();
        Map<String, List<DataFile>> dataFiles = new DataFileReader().readFiles("D:\\data-shared", "20190919.csv");

        for (String symbol : dataFiles.keySet()) {
            List<DataFile> symbolDataFiles = dataFiles.get(symbol);
            for (int i = 0; i < symbolDataFiles.size(); i++) {
                for (int j = i + 1; j < symbolDataFiles.size(); j++) {
                    DataFile dataFile1 = symbolDataFiles.get(i);
                    DataFile dataFile2 = symbolDataFiles.get(j);
                    int leadLag = crossCorrelator.crossCorrelate(dataFile1.getDeltaSignal(), dataFile2.getDeltaSignal());

                    System.out.println(String.format(
                            "%s,%s,%s,%d",
                            symbol,
                            dataFile1.getFeed(),
                            dataFile2.getFeed(),
                            leadLag * 500)); //  500 microseconds per time series data point
                }
            }
        }
    }

    private static void test() {

        double[] signal1 = new double[] {1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0};
        double[] signal2 = new double[] {1.0, 1.0, 1.0, 1.0, 0.0, 0.0, 0.0, 0.0};
        double[] signal3 = new double[] {1.0, 3.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0};
        double[] signal4 = new double[] {1.0, 3.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0};
        double[] signal5  = new double[] {0.0, 0.1, 1.1, 2.9, 0.9, 0.0, 0.0, 0.0};

        // +ve lag means second input was behind the first one, -ve is the other way around
        //   eg: signal3 vs signal5 => max at +2
        //   eg: signal5 vs signal3 => max at -2
        System.out.println("LEAD LAG: " + new CrossCorrelator().crossCorrelate(signal3, signal4)); // same
        System.out.println("LEAD LAG: " + new CrossCorrelator().crossCorrelate(signal3, signal5)); // 1st is ahead of 2nd by 2
        System.out.println("LEAD LAG: " + new CrossCorrelator().crossCorrelate(signal5, signal3)); // 2nd is ahead of 1st by 2
    }
}

